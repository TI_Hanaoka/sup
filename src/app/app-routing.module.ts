import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'product-list', loadChildren: './product-list/product-list.module#ProductListModule'},
  {path: 'top', loadChildren: './top/top.module#TopModule'},
  {path: '**', redirectTo: 'top'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
