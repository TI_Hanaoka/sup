import { Component, OnInit } from '@angular/core';
import {ProductListService} from '../../core/service/product-list.service';
import {IProduct} from '../../shard/interfaces/iproduct';

@Component({
  selector: 'sup-product-list-page',
  templateUrl: './product-list-page.component.html',
  styleUrls: ['./product-list-page.component.scss']
})
export class ProductListPageComponent implements OnInit {

  products: IProduct[];

  constructor(private productListService: ProductListService) { }

  ngOnInit() {
    this.getProductList();
  }

  onProductListButton() {
    this.getProductList();
  }

  getProductList() {
    this.productListService.getProductList().subscribe( res => {
      this.products = res;
    });
  }
}
