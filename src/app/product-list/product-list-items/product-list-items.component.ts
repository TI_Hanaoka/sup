import {Component, Input, OnInit} from '@angular/core';
import {IProduct} from '../../shard/interfaces/iproduct';

@Component({
  selector: 'sup-product-list-items',
  templateUrl: './product-list-items.component.html',
  styleUrls: ['./product-list-items.component.scss']
})
export class ProductListItemsComponent implements OnInit {

  @Input() product: IProduct;

  constructor() { }

  ngOnInit() {
  }

}
