import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductListItemsComponent } from './product-list-items.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {By} from '@angular/platform-browser';
import {IProduct} from '../../shard/interfaces/iproduct';

describe('ProductListItemsComponent', () => {
  let component: ProductListItemsComponent;
  let fixture: ComponentFixture<ProductListItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductListItemsComponent
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  const productDummy: IProduct = {
    id: 1,
    title: 'title_1',
    body: 'body_1',
    pages: 111,
    publisher: 'publisher_1',
    language: 'language_1',
    isbn10: 'isbn10_1',
    isbn13: 'isbn13_1',
    releaseDate: 'releaseDate_1',
    packingSize: 'packingSize_1',
    price: 1000
  };

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListItemsComponent);
    component = fixture.componentInstance;
    component.product = productDummy;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('商品一覧の項目が表示される', () => {

    it('商品一覧のタイトルが表示される', () => {
      const de = fixture.debugElement.query(By.css('span.title'));
      const ne = de.nativeElement;
      expect(ne.innerText).toEqual('title_1');
    });

    it('商品一覧の本文が表示される', () => {
      const de = fixture.debugElement.query(By.css('span.body'));
      const ne = de.nativeElement;
      expect(ne.innerText).toEqual('body_1');
    });

    it('商品一覧の価格が日本円表記で表示される', () => {
      const de = fixture.debugElement.query(By.css('span.price'));
      const ne = de.nativeElement;
      expect(ne.innerText).toEqual('¥1,000-');
    });
  });

  describe('商品一覧の各カード内にボタンが表示されている', () => {
    it('「カートに入れる」ボタンが表示されている', () => {
      const de = fixture.debugElement.query(By.css('button.add-cart'));
      const ne = de.nativeElement;
      expect(ne.innerText).toEqual('カートに入れる');
    });

    it('「詳細」ボタンが表示されている', () => {
      const de = fixture.debugElement.query(By.css('button.detail'));
      const ne = de.nativeElement;
      expect(ne.innerText).toEqual('詳細');
    });
  });
});
