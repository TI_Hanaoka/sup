import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductListRoutingModule } from './product-list-routing.module';
import {ProductListPageComponent} from './product-list-page/product-list-page.component';
import {ShardModule} from '../shard/shard.module';
import { ProductListItemsComponent } from './product-list-items/product-list-items.component';

@NgModule({
  declarations: [ProductListPageComponent, ProductListItemsComponent],
  imports: [
    CommonModule,
    ShardModule,
    ProductListRoutingModule
  ]
})
export class ProductListModule { }
