import {IProduct} from '../interfaces/iproduct';

export class Product implements IProduct {
  constructor(
    public id: number,
    public title: string,
    public body: string,
    public pages: number,
    public publisher: string,
    public language: string,
    public isbn10: string,
    public isbn13: string,
    public releaseDate: string,
    public packingSize: string,
    public price: number
  ) {
  }
}
