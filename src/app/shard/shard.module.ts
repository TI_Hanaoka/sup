import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import {ShardRoutingModule} from './shard-routing.module';
import { CardStyleComponent } from './card-style/card-style.component';

@NgModule({
  declarations: [
    HeaderComponent,
    SideMenuComponent,
    CardStyleComponent
  ],
  imports: [
    CommonModule,
    ShardRoutingModule
  ],
  exports: [
    HeaderComponent,
    SideMenuComponent,
    CardStyleComponent
  ]
})
export class ShardModule { }
