import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideMenuComponent } from './side-menu.component';
import {By} from '@angular/platform-browser';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('SideMenuComponent', () => {
  let component: SideMenuComponent;
  let fixture: ComponentFixture<SideMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [
        SideMenuComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('サイドメニューに「商品一覧」が表示されている', () => {
    const de = fixture.debugElement.query(By.css('span.productList'));
    const ne = de.nativeElement;
    expect(ne.innerText).toEqual('商品一覧');
  });

  it('サイドメニューに「カート」が表示されている', () => {
    const de = fixture.debugElement.query(By.css('span.cart'));
    const ne = de.nativeElement;
    expect(ne.innerText).toEqual('カート');
  });

  it('サイドメニューに「購入履歴」が表示されている', () => {
    const de = fixture.debugElement.query(By.css('span.purchaseHistory'));
    const ne = de.nativeElement;
    expect(ne.innerText).toEqual('購入履歴');
  });

  it('サイドメニューに「ログアウト」が表示されている', () => {
    const de = fixture.debugElement.query(By.css('span.logout'));
    const ne = de.nativeElement;
    expect(ne.innerText).toEqual('ログアウト');
  });

  it('商品一覧ボタンを押下時、productListイベントを発行する', () => {
    component.onClickProductList();
    component.productList.subscribe( res => {
      console.log(res);
      expect(res).toBeUndefined();
    });
  });
});
