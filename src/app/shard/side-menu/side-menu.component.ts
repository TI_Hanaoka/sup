import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ProductListService} from '../../core/service/product-list.service';

@Component({
  selector: 'sup-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  @Output() productList: EventEmitter<{}> = new EventEmitter<{}>();

  constructor() { }

  ngOnInit() {
  }

  onClickProductList() {
    this.productList.emit();
  }

  onClickCart() {
  }

  onClickPurchaseHistory() {
  }

  onClickLogout() {
  }
}
