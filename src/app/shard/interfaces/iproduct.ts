export interface IProduct {
  id: number;
  title: string;
  body: string;
  pages: number;
  publisher: string;
  language: string;
  isbn10: string;
  isbn13: string;
  releaseDate: string;
  packingSize: string;
  price: number;
}
