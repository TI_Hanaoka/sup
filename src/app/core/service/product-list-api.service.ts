import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {IProduct} from '../../shard/interfaces/iproduct';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductListApiService {

  url = `${environment.apiHost}/api/products`;

  constructor(private httpClient: HttpClient) { }

  get(): Observable<IProduct[]> {
    return this.httpClient.get<IProduct[]>(this.url);
  }
}
