import { Injectable } from '@angular/core';
import {ProductListApiService} from './product-list-api.service';
import {IProduct} from '../../shard/interfaces/iproduct';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductListService {

  constructor(private productListApiService: ProductListApiService) { }

  getProductList(): Observable<IProduct[]> {
    return this.productListApiService.get();
  }

}
