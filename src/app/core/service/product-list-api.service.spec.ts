import { TestBed } from '@angular/core/testing';

import { ProductListApiService } from './product-list-api.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {IProduct} from '../../shard/interfaces/iproduct';

describe('ProductListApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ]
  }));

  let testTarget: ProductListApiService;
  let backEnd: HttpTestingController;

  beforeEach( () => {
    testTarget = TestBed.get(ProductListApiService);
    backEnd = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(testTarget).toBeTruthy();
  });

  describe('商品一覧を取得する' , () => {
    const url = '/api/products';

    it('リクエストURLとメソッドが正しい', () => {
      testTarget.get().subscribe();
      const mockServer = backEnd.expectOne(url);
      expect(mockServer.request.url).toEqual(url);
      expect(mockServer.request.method).toEqual('GET');
      mockServer.flush({});
    });

    it('1回のリクエストに対するレスポンスが正しく返答される', () => {
      const productsDummy: IProduct[] = [
        {
          id: 1,
          title: 'title_1',
          body: 'body_1',
          pages: 111,
          publisher: 'publisher_1',
          language: 'language_1',
          isbn10: 'isbn10_1',
          isbn13: 'isbn13_1',
          releaseDate: 'releaseDate_1',
          packingSize: 'packingSize_1',
          price: 1000
        },
        {
          id: 2,
          title: 'title_2',
          body: 'body_2',
          pages: 222,
          publisher: 'publisher_2',
          language: 'language_2',
          isbn10: 'isbn10_2',
          isbn13: 'isbn13_2',
          releaseDate: 'releaseDate_2',
          packingSize: 'packingSize_2',
          price: 2000
        }
      ];

      testTarget.get().subscribe((res: IProduct[]) => {
        expect(res).toEqual(productsDummy);
      });
      const mockServer = backEnd.expectOne(url);
      mockServer.flush(productsDummy);
    });
  });

});
