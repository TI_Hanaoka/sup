import { TestBed } from '@angular/core/testing';

import { ProductListService } from './product-list.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ProductListApiService} from './product-list-api.service';
import {of} from 'rxjs';
import {IProduct} from '../../shard/interfaces/iproduct';

describe('ProductListService', () => {
  const apiClientStub = {
    get: () => {
      return of({});
    }
  };

  beforeEach(() => TestBed.configureTestingModule({
    // imports: [
    //   HttpClientTestingModule
    // ],
    providers: [
      ProductListService,
      {provide: ProductListApiService, useValue: apiClientStub}
    ]
  }));

  let productListApiService: ProductListApiService;
  let testTarget: ProductListService;

  beforeEach(() => {
    productListApiService = TestBed.get(ProductListApiService);
    testTarget = TestBed.get(ProductListService);
  });

  it('should be created', () => {
    expect(testTarget).toBeTruthy();
  });

  describe('商品一覧を確認できる', () => {
    it('商品一覧が取得できる', () => {
      const productsDummy: IProduct[] = [
        {
          id: 1,
          title: 'title_1',
          body: 'body_1',
          pages: 111,
          publisher: 'publisher_1',
          language: 'language_1',
          isbn10: 'isbn10_1',
          isbn13: 'isbn13_1',
          releaseDate: 'releaseDate_1',
          packingSize: 'packingSize_1',
          price: 1000
        },
        {
          id: 2,
          title: 'title_2',
          body: 'body_2',
          pages: 222,
          publisher: 'publisher_2',
          language: 'language_2',
          isbn10: 'isbn10_2',
          isbn13: 'isbn13_2',
          releaseDate: 'releaseDate_2',
          packingSize: 'packingSize_2',
          price: 2000
        }
      ];
      const spy = spyOn(productListApiService, 'get').and.callFake(() => of(productsDummy));
      testTarget.getProductList().subscribe( res => {
        expect(res).toEqual(productsDummy);
        expect(spy.calls.count()).toEqual(1);
      });
    });
  });
});
