import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TopRoutingModule } from './top-routing.module';
import { TopPageComponent } from './top-page/top-page.component';
import {ShardModule} from '../shard/shard.module';

@NgModule({
  declarations: [TopPageComponent],
  imports: [
    CommonModule,
    ShardModule,
    TopRoutingModule
  ]
})
export class TopModule { }
