import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopPageComponent } from './top-page.component';
import {Component} from '@angular/core';

@Component({
  selector: 'sup-header',
  template: ''
})
class HeaderStubComponent {}

@Component({
  selector: 'sup-side-menu',
  template: ''
})
class SideMenuStubComponent {}

describe('TopPageComponent', () => {
  let component: TopPageComponent;
  let fixture: ComponentFixture<TopPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TopPageComponent,
        HeaderStubComponent,
        SideMenuStubComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
