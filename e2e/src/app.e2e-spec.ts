import { AppPage } from './app.po';
import {E2eUtil} from './e2e.util';

describe('workspace-project App', () => {
  let page: AppPage;
  let util: E2eUtil;

  beforeEach(() => {
    page = new AppPage();
    util = new E2eUtil();
  });

  it('should display message', () => {
    page.navigateTo();
    expect(page.getProductListTextFromSideMenu()).toEqual('商品一覧');
    expect(page.getCartTextFromSideMenu()).toEqual('カート');
    expect(page.getPurchaseHistoryTextFromSideMenu()).toEqual('購入履歴');
    expect(page.getLogoutTextFromSideMenu()).toEqual('ログアウト');
    expect(util.getCurrentUrlPath()).toEqual('/top');
  });
});
