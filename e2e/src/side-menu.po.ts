import {browser, by, element, ElementArrayFinder} from 'protractor';

export class SideMenuPage {
  navigateTo() {
    return browser.get('/product-list');
  }

  getNavigationLinks(): ElementArrayFinder {
    return element.all(by.css('a'));
  }

  getProductListLink() {
    return this.getNavigationLinks().get(0);
  }

  getLunchPlanningText() {
    return this.getProductListLink().getText();
  }

  clickProductListLink() {
    return this.getProductListLink().click();
  }
}
