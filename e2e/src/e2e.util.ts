import { browser } from 'protractor';

export class E2eUtil {

  getCurrentUrlPath() {
    return browser.getCurrentUrl().then(url => this.removeOrigin(url));
  }
  removeOrigin(url: string) {
    return url.replace(/^https?:\/\/[^/]+/, '');
  }
}
