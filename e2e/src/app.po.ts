import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getProductListTextFromSideMenu() {
    return element(by.css('sup-root span.productList')).getText();
  }

  getCartTextFromSideMenu() {
    return element(by.css('sup-root span.cart')).getText();
  }

  getPurchaseHistoryTextFromSideMenu() {
    return element(by.css('sup-root span.purchaseHistory')).getText();
  }

  getLogoutTextFromSideMenu() {
    return element(by.css('sup-root span.logout')).getText();
  }
}
