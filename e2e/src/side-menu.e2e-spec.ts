import {E2eUtil} from './e2e.util';
import {SideMenuPage} from './side-menu.po';
import {AppPage} from './app.po';

describe('workspace-project App', () => {
  let sideMenuPage: SideMenuPage;
  let appPage: AppPage;
  let util: E2eUtil;

  beforeEach(() => {
    sideMenuPage = new SideMenuPage();
    appPage = new AppPage();
    util = new E2eUtil();
  });

  it('トップ画面から商品一覧ボタンを押下時、URLが”/product-list”に変化する', () => {
    appPage.navigateTo();
    expect(util.getCurrentUrlPath()).toEqual('/top');
    expect(sideMenuPage.getLunchPlanningText()).toEqual('商品一覧');
    sideMenuPage.clickProductListLink();
    expect(util.getCurrentUrlPath()).toEqual('/product-list');
  });
});
